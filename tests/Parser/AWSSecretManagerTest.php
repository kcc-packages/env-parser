<?php
use PHPUnit\Framework\TestCase;

class AWSSecretManagerTest extends TestCase
{
    protected $awsAccessKeyId = 'AKIA...';
    protected $awsSecretAccessKey = '13456789...40';
    protected $secretRegion = 'us-east-2';
    protected $secretId = 'MiSecreto';

    /**
     * @expectedException Exception
     */
    public function testEmptyEnvironment()
    {
        putenv('AWS_ACCESS_KEY_ID');
        putenv('AWS_SECRET_ACCESS_KEY');
        $client = new \KCCPackages\EnvParser\Parser\AWSSecretManager($this->secretRegion, $this->secretId);
        $this->expectException($client);
    }

    /**
     * @expectedException Exception
     */
    public function testEmptyParams()
    {
        putenv('AWS_ACCESS_KEY_ID=' . $this->awsAccessKeyId);
        putenv('AWS_SECRET_ACCESS_KEY=' . $this->awsSecretAccessKey);
        $client = new \KCCPackages\EnvParser\Parser\AWSSecretManager('', '');
        $this->expectException($client);
    }

    /**
     * Claves AWS incorrectas
     * @expectedException Exception
     */
    public function testNoEmpty()
    {
        putenv('AWS_ACCESS_KEY_ID=' . $this->awsAccessKeyId);
        putenv('AWS_SECRET_ACCESS_KEY=' . $this->awsSecretAccessKey);
        $client = new \KCCPackages\EnvParser\Parser\AWSSecretManager($this->secretRegion, $this->secretId);
        $this->expectException($client);
    }
}
