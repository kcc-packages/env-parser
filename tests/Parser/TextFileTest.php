<?php
use PHPUnit\Framework\TestCase;

class TextFileTest extends TestCase
{
    protected $emptyFilePath = "./tests/assets/empty.txt";
    protected $noValuesFilePath = "./tests/assets/no-values.txt";
    protected $valuesFilePath = "./tests/assets/values.txt";

    /**
     * @expectedException Exception
     */
    public function testEmpty()
    {
        $TextFile = new \KCCPackages\EnvParser\Parser\TextFile($this->emptyFilePath);
        $data = $TextFile->read();
        $this->assertArrayNotHasKey('KEY1', $data, "La clave KEY1 no debe existir en el archivo de texto.");
        $this->expectException($TextFile);
    }

    public function testNoValues()
    {
        $TextFile = new \KCCPackages\EnvParser\Parser\TextFile($this->noValuesFilePath);
        $data = $TextFile->read();
        $this->assertArrayHasKey('KEY1', $data, "La clave KEY1 debe existir en el archivo de texto.");
        $this->assertEquals('', $data['KEY1'], "La clave KEY1 debe estar vac�a.");
    }

    public function testValues()
    {
        $TextFile = new \KCCPackages\EnvParser\Parser\TextFile($this->valuesFilePath);
        $data = $TextFile->read();
        $this->assertArrayHasKey('KEY1', $data, "La clave KEY1 debe existir en el archivo de texto.");
        $this->assertEquals('value1', $data['KEY1'], "La clave KEY1 debe ser igual a 'value1'.");
    }

    public function writeToEnvTest()
    {
        $TextFile = new \KCCPackages\EnvParser\Parser\TextFile($this->valuesFilePath);
        $TextFile->writeToEnv();
        $this->assertEquals('value1', getenv('KEY1'), "La clave KEY1 debe existir en el entorno y ser igual a 'value1'.");
    }
}
