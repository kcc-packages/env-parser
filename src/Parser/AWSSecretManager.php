<?php
namespace KCCPackages\EnvParser\Parser;

use Aws\SecretsManager\SecretsManagerClient;
use KCCPackages\EnvParser\Parser;

/**
 * Class AWSSecretManager
 * El m�todo parse() requiere que est�n definidas en el entorno:
 * - AWS_ACCESS_KEY_ID - Identificador de acceso (suele comenzar con AKIA...).
 * - AWS_SECRET_ACCESS_KEY - Clave
 * @package KCCPackages\EnvParser\Parser
 */
class AWSSecretManager extends Parser
{
    const ORIGIN = 'AWS';

    const ERROR_AWS_KEYS = "Las claves de AWS no est�n definidas en el entorno.";
    const ERROR_PARAM = "Faltan datos obligatorios para instanciar la clase AWSSecretManager.";

    /**
     * AWSSecretManager constructor.
     * @param string $region Regi�n de AWS. Ej: 'us-east-2'.
     * @param string $secretName Nombre del secreto. Ej: 'MiClaveSecreta'
     * @throws \Exception
     */
    function __construct($region = '', $secretName = '')
    {
        try {
            if (empty($region) || empty($secretName)) {
                throw new \Exception(self::ERROR_PARAM);
            }
            if (!getenv('AWS_ACCESS_KEY_ID') || !getenv('AWS_SECRET_ACCESS_KEY')) {
                throw new \Exception(self::ERROR_AWS_KEYS);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        parent::$origin = self::ORIGIN;
        $this->parsedData = $this->parse($region, $secretName);
        return;
    }

    /**
     * Instancia el cliente AWS Secret Manager y devuelve su contenido en un array.
     * @param string $region
     * @param string $secretName
     * @return array
     * @throws \Exception
     */
    public function parse($region = '', $secretName = ''): array
    {
        try {
            $client = new SecretsManagerClient([
                'version' => 'latest',
                'region'  => $region,
            ]);
            $result = $client->getSecretValue(['SecretId' => $secretName]);
        } catch (\Exception $e) {
            throw $e;
        }
        return json_decode($result['SecretString'], true);
    }
}
