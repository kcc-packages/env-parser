<?php
namespace KCCPackages\EnvParser\Parser;

use KCCPackages\EnvParser\Parser;

class Session extends Parser
{
    const ORIGIN = 'SESSION';

    private $prefix;

    function __construct($prefix = 'ENV_')
    {
        $this->prefix = $prefix;
        $this->parsedData = $this->parse($_SESSION);
        parent::$origin = self::ORIGIN;
    }

    public function parse($dataToParse = []): array
    {
        $parsedData = [];
        array_walk($dataToParse, function ($value, $key) use (&$parsedData) {
            if (strpos($key, $this->prefix) !== false) {
                $parsedData[substr($key, strlen($this->prefix))] = $value;
            }
        });
        return $parsedData;
    }

    public function writeFrom($parsedData = []): void
    {
        $this->parsedData = $parsedData;
        array_walk($parsedData, function ($value, $key) {
            $_SESSION[$this->prefix . $key] = $value;
        });
        return;
    }

    public function wipe()
    {
        array_walk(array_keys($_SESSION), function ($key) {
            if (strpos($key, $this->prefix) !== false) {
                unset($_SESSION[$key]);
            }
        });
    }
}
