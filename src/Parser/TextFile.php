<?php
namespace KCCPackages\EnvParser\Parser;

use KCCPackages\EnvParser\Parser;

class TextFile extends Parser
{
    const ORIGIN = 'TEXTFILE';

    const ERROR_NO_FILENAME = "No ha indicado nombre de archivo de variables de entorno.";
    const ERROR_NO_FILE = "No existe el archivo de variables de entorno {FILENAME}.";
    const ERROR_EMPTY_FILE = "El archivo de variables de entorno est� vac�o.";

    function __construct($filename = '')
    {
        try {
            if (empty($filename)) {
                throw new \Exception(self::ERROR_NO_FILENAME);
            }
            if (!is_readable($filename)) {
                throw new \Exception(str_replace('{FILENAME}', $filename, self::ERROR_NO_FILE));
            }
            $dataToParse = file($filename);
            if (empty($dataToParse)) {
                throw new \Exception(self::ERROR_EMPTY_FILE);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        parent::$origin = self::ORIGIN;
        $this->parsedData = $this->parse($dataToParse);
    }

    public function parse($dataToParse = []): array
    {
        $parsedData = [];
        foreach ($dataToParse as $line) {
            $line = trim($line); // elimina retornos de carro al final de la l�nea
            if (strpos($line, '=') !== false) {
                $parts = explode('=', $line);
                $key = array_shift($parts);
                $value = !empty($parts) ? implode('=', $parts) : '';
                $parsedData[$key] = $value;
            }
        }
        return $parsedData;
    }
}
