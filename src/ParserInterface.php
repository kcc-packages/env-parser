<?php
namespace KCCPackages\EnvParser;

interface ParserInterface
{
    // Lee desde el origen y archiva en un array local
    public function parse(): array;

    // Lee desde el array local y escribe en el entorno
    public function writeToEnv(): void;

    // Verifica si la variable existe en el array local
    public function exists($var = ''): bool;

    // Devuelve el array local
    public function read(): array;

    public function getOrigin(): string;
}
