<?php
namespace KCCPackages\EnvParser;

abstract class Parser implements ParserInterface
{
    static $origin;

    protected $parsedData;

    public function parse(): array
    {
        return $this->parsedData;
    }

    public function writeToEnv(): void
    {
        if (empty($this->parsedData)) {
            return;
        }
        array_walk($this->parsedData, function ($value, $key) {
            putenv($key . '=' . $value);
        });
        return;
    }

    public function exists($key = ''): bool
    {
        return !empty($key) && isset($this->parsedData[$key]);
    }

    public function read(): array
    {
        return $this->parsedData;
    }

    public function getOrigin(): string
    {
        return self::$origin;
    }
}
