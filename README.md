.env parser
===

Gestión de variables de entorno.

Utilización desde archivo de texto plano.
---

Archivo **.env**:

```
CLAVE1=Valor 1
CLAVE2=Valor 2
```

Utilización desde PHP:

```php
require 'vendor/autoload.php';

use KCCPackages\EnvParser\TextFile;

$filename = './.env';
$parser = new TextFile($filename);
$parser->writeToEnv();

echo getenv('CLAVE1'); // "Valor 1"
```

Utilización desde una ubicación en AWS Secret Manager.
---

Se requiere utilizar la clase **Aws\SecretsManager\SecretsManagerClient** incluida en el SDK de AWS.
Para instanciar el cliente de Secret Manager, las siguientes claves deben existir en el entorno:

* `AWS_ACCESS_KEY_ID` - Identificador de acceso (suele comenzar con `AKIA...`).
* `AWS_SECRET_ACCESS_KEY` - Clave alfanumérica (suele ser de 40 caracteres).

```php
require 'vendor/autoload.php';

use KCCPackages\EnvParser\AWSSecretManager;

$awsSecretRegion = 'us-east-2'; // Región AWS del secreto
$awsSecretId = 'MiSecreto'; // Nombre del secreto
$parser = new AWSSecretManager($awsSecretRegion, $awsSecretId);
$parser->writeToEnv();
```

Utilización desde variables de sesión
---

Lee desde la sesión las variables que comiencen con `ENV_` y las carga en el entorno.

```php
require 'vendor/autoload.php';

use KCCPackages\EnvParser\Session;

$parser = new Session();
$parser->writeToEnv();
```

Escenario combinado
---

Este ejemplo combina las opciones anteriores:

1. Se consulta las credenciales desde la sesión de PHP.
2. Si no existen:
    * se obtienen las credenciales para AWS Secret Manager desde un archivo de texto.
    * Se cargan las credenciales desde Secret Manager hacia la sesión.
3. Se cargan desde la sesión hacia el entorno.

```php
require 'vendor/autoload.php';

use KCCPackages\EnvParser\TextFile;
use KCCPackages\EnvParser\Session;
use KCCPackages\EnvParser\AWSSecretManager;

$sessionParser = new EnvParser\Session();
if (!$sessionParser->exists('DB_HOST')) {
    $textParser = new EnvParser\TextFile('.env');
    $textParser->writeToEnv();
    $secretsParser = new EnvParser\AWSSecretManager('us-east-2', 'DatabaseCredentials');
    $secretsParser->writeToEnv();
    $sessionParser->writeFrom($secretsParser->get());
} else {
    $sessionParser->writeToEnv();
}
echo getenv('DB_HOST); // "..."
```
